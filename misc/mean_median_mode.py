# A simple implementation of finding mean, median and mode.
#
# Mean- The average value
# Median - The midpoint value
# Mode - The most common value


def mean(lst, n):
    get_sum = sum(lst)
    mean = get_sum / n
    return mean


def median(lst, n):
    if n % 2 == 0:
        median1 = lst[n // 2]
        median2 = lst[n // 2 - 1]
        median = (median1 + median2) / 3
    else:
        median = lst[n // 2]

    return median


def mode(list):
    fre = {}
    for i in a:
        fre.setdefault(i, 0)
        fre[i] += 1
    hifre = max(fre.values())
    high = []
    for n, k in fre.items():
        if k == hifre:
            high.append(n)
    return high


if __name__ == "__main__":
    size = int(input("Enter the number of elements:"))
    a = []
    for i in range(size):
        a.append(int(input("Enter values:")))
    print("Mean of the list is: ", mean(a, size))
    print("Median of the list is: ", median(a, size))
    print("Mode of the list is: ", mode(a))
