#include <stdio.h>

int
main ()
{
    int res = 0, num, on, temp;
    printf ("Enter number to check: ");
    scanf ("%d", &num);
    on = num;
    while (num > 0)
        {
            temp = num % 10;
            res = res + (temp * temp * temp);
            num = num / 10;
        }
    if (on == res)
        {
            printf ("Armstrong Number");
        }
    else
        {
            printf ("Not a Armstrong Number");
        }
}
