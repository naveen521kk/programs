#include <stdio.h>

#define PI 3.141592
int
main ()
{
    float r, area, circum;
    printf ("Enter radius of the circle: ");
    scanf ("%f", &r);
    area = PI * r * r;
    circum = 2 * PI * r;
    printf ("Area of circle of radius %f is: %f\n", r, area);
    printf ("Circumference of circle %f is: %f\n", r, circum);
}
