# Tower of Hanoi, is a mathematical puzzle which consists of three towers
# (pegs) and more than one rings is as depicted.


def tower_of_hanoi(n, source, destination, auxiliary):

    if n == 1:
        print("Move disk 1 from source", source, "to destination", destination)
        return
    tower_of_hanoi(n - 1, source, auxiliary, destination)
    print("Move disk", n, "from source", source, "to destination", destination)
    tower_of_hanoi(n - 1, auxiliary, destination, source)


if __name__ == "__main__":
    n = int(input("Enter the number of disks"))
    tower_of_hanoi(n, "A", "B", "C")
