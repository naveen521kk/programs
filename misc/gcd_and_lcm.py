# program for computing gcd, lcm of two numbers using python


def gcd(a, b):
    res = 1
    for i in range(1, a + 1):
        if a % i == 0 and b % i == 0:
            res = i
    return res


if __name__ == "__main__":
    first = int(input("Enter first number: "))
    second = int(input("Enter second number: "))

    # function call for gcd

    print(f"HCF or GCD of {first} and {second} is {gcd(first,second)}")
    # function call for lcm
    lcm = first * second / gcd(first, second)
    print(f"LCM of {first} and {second} is {lcm}")
