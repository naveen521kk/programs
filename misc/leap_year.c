// Algorithm for leap year from
// https://docs.microsoft.com/en-us/office/troubleshoot/excel/determine-a-leap-year#how-to-determine-whether-a-year-is-a-leap-year

#include <stdio.h>

int
main ()
{
    int yr;
    printf ("Year to check: ");
    scanf ("%d", &yr);
    if (yr % 4 == 0)
        {
            if (yr % 100 == 0)
                {
                    if (yr % 400 == 0)
                        {
                            printf ("Leap year");
                        }
                    else
                        {
                            printf ("Not a leap year");
                        }
                }
            else
                {
                    printf ("Leap Year");
                }
        }
    else
        {
            printf ("Not a leap year");
        }
}
