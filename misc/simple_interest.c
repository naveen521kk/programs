#include <stdio.h>

int
main ()
{
    float P, R, T, SI;
    printf ("Enter principle: ");
    scanf ("%f", &P);
    printf ("Enter interest rate: ");
    scanf ("%f", &R);
    printf ("Enter number of years: ");
    scanf ("%f", &T);
    SI = (P * R * T) / 100;
    printf ("Simple interest is: %f", SI);
}
