# Print the fibonacci series upto `n' numbers taken as a parameter
# The Fibonacci numbers, commonly denoted Fn, form a sequence, the
# Fibonacci sequence, in which each number is the sum of the two
# preceding ones.

# Implementation Detail: The below implmentation is using loops


def print_fibonacci_series(n: int):
    """
    n -> number of items to print
    """
    n1, n2 = 0, 1  # series start with 0
    fib_nums = [n1, n2]
    for _ in range(2, n):  # already 2 numbers done
        n3 = n1 + n2
        fib_nums.append(n3)
        n1 = n2
        n2 = n3
    return fib_nums


if __name__ == "__main__":
    a = print_fibonacci_series(16)
    for i in a:
        print(i, end=" ")
