# An anagram of a string is another string that contains the same characters,
# only the order of characters can be different. For example, “abcd” and
# “dabc” are an anagram of each other.


def are_anagram(str1, str2):
    # Get lengths of both strings
    n1 = len(str1)
    n2 = len(str2)

    # If length of both strings is not same, then they cannot be anagram
    if n1 != n2:
        return 0

    # Sort both strings
    str1 = sorted(str1)
    str2 = sorted(str2)

    # Compare sorted strings
    for i in range(0, n1):
        if str1[i] != str2[i]:
            return 0
    return 1


if __name__ == "__main__":

    str1 = input("Enter the First String:")
    str2 = input("Enter the Second String:")

    # lower() - converts the string to lower case

    str1 = str1.lower()
    str2 = str2.lower()

    # Function Call

    if are_anagram(str1, str2):
        print("The two strings are anagram of each other")
    else:
        print("The two strings are not anagram of each other")
