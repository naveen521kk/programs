# Find all permutations of a given sequence of non-repetitive set of values
# Permutations is the act of arranging the given elements to form different
# but unique values.
#
# This program achieves it by using recursion.
# Orignal Author: Naveen M K


def permutations(orig, prev=[]):
    if len(orig) == 1:
        return "".join(prev + orig)
    tmp = []
    for it, _n in enumerate(orig):
        new = permutations(orig[:it] + orig[it + 1 :], prev + [_n])
        if type(new) == list:
            tmp.extend(new)
        else:
            tmp.append(new)
    return tmp


if __name__ == "__main__":
    val = input("Enter values to find permutations: ")
    print(permutations(list(val)))
