# Find sum of elements in a Array(list) using iterative method


def sum_of_items_list(lnums):
    sum = 0
    for i in range(len(lnums)):
        sum = sum + lnums[i]
    return sum


if __name__ == "__main__":
    lnums = [1, 3, 5, 6, 7]
    res = sum_of_items_list(lnums)
    print(res)
