# Find sum of elements in a Array(list) using recursive method


def sum(num_list):
    if len(num_list) == 1:
        return num_list[0]
    else:
        return num_list[0] + sum(num_list[1:])


if __name__ == "__main__":
    my_list = [1, 3, 5, 7, 9]
    print(sum(my_list))
