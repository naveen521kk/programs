# Find square root of a number using Einstein method.


def sqrt(num):
    x = 2 * num
    while True:
        y = (x + num / x) / 2
        if y == x:
            break
        x = y
    print("Square root of", num, "is", y)


if __name__ == "__main__":
    sqrt(25)
