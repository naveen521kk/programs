def pow(x, y):
    powered = x
    if y == 0:
        powered = 1
    else:
        while y > 1:
            powered *= x
            y -= 1
    return powered


if __name__ == "__main__":
    a = 4
    b = 3
    print(a, "power", b, "is", pow(a, b))
